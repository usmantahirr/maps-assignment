# How to Install

Install Gulp & Bower
```bash
sudo npm install -g gulp-cli bower
```

Install Dependenies
```
npm install && bower install
```

give command on cli
```
gulp serve
```

and you'll se a server on localhost:3000

#### Complete
Map View & Current Usage are complete
- In Map View, click on marker to see details
- In Currunt Usage, click on Marker to see current Usage.
- Search bar complete with auto complete
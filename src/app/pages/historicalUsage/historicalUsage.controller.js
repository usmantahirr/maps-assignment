(function() {
  'use strict';

  angular
    .module('gmpsAssignment')
    .controller('HistoricalUsageController', HistoricalUsageController);

  /** @ngInject */
  function HistoricalUsageController($log) {
    var vm = this;

    $log.debug(vm);
    $log.debug('HistoricalUsageController');
  }
})();

(function() {
  'use strict';

  angular
    .module('gmpsAssignment')
    .controller('MapViewController', MapViewController);

  /** @ngInject */
  function MapViewController($log, Station, MapConfFactory) {
    var vm = this;

    vm.stations = Station.all;
    vm.detail = {
      visible: false,
      content: {}
    };

    vm.mapConf = MapConfFactory.getConf();
    vm.getIcon = MapConfFactory.getIcon;

    vm.clickHandler = function(event, station) {
      vm.detail.visible = true;
      vm.detail.content = station;
    };

    vm.getDate = function () {
      var date = new Date(vm.detail.content.last_reported * 1000);
      return date.toGMTString();
    };

    vm.hide = function () {
      vm.detail.visible = false;
      vm.detail.content = NaN;
    };

    vm.getClass = function (percentage) {
      if (percentage === 0) {
        return 'label-danger'
      } else if (percentage < 50) {
        return 'label-warning'
      }
      return 'label-success'
    };
  }
})();

(function() {
  'use strict';

  angular
    .module('gmpsAssignment')
    .controller('CurrentUsageController', CurrentUsageController);

  /** @ngInject */
  function CurrentUsageController($log, $scope, Station, MapConfFactory) {
    var vm = this;

    vm.stations = Station.all;
    vm.usage = Station.globalUsage;

    $log.debug(vm.usage);

    vm.detail = {
      visible: false,
      content: {},
      gauge: {
        col: [{ id: 'In Transit', type: 'gauge' }],
        data: [{ 'In Transit': 0 }]
      }
    };

    vm.cb = function (chart) {
      vm.chart = chart;
    };

    vm.mapConf = MapConfFactory.getConf();
    vm.getIcon = MapConfFactory.getIcon;

    vm.clickHandler = function(event, station) {
      vm.detail.visible = true;
      vm.detail.content = station;
      vm.detail.gauge.data[0]['In Transit'] = station.used;

      if (vm.chart) {
        vm.chart.internal.config.gauge_max = station.capacity;
      }
    };

    vm.hide = function () {
      vm.detail.visible = false;
      vm.detail.content = NaN;
    };

    vm.getClass = function (percentage) {
      if (percentage === 0) {
        return 'label-danger'
      } else if (percentage < 50) {
        return 'label-warning'
      }
      return 'label-success'
    };
  }
})();

(function() {
  'use strict';

  angular
    .module('gmpsAssignment')
    .controller('DistanceQueryController', DistanceQueryController);

  /** @ngInject */
  function DistanceQueryController($scope, Station, MapConfFactory) {
    var vm = this;

    vm.stations = Station.all;
    vm.detail = {
      visible: false,
      content: {}
    };

    $scope.$watch('dq.selected', function (nv) {
      if (nv) {
        vm.detail.visible = true;
        vm.detail.content = nv;
      }
    });

    vm.mapConf = MapConfFactory.getConf();
    vm.getIcon = MapConfFactory.getIcon;

    vm.clickHandler = function(event, station) {
      vm.detail.visible = true;
      vm.detail.content = station;
    };

    vm.hide = function () {
      vm.detail.visible = false;
      vm.detail.content = NaN;
    };

    vm.getClass = function (percentage) {
      if (percentage === 0) {
        return 'label-danger'
      } else if (percentage < 50) {
        return 'label-warning'
      }
      return 'label-success'
    };
  }
})();

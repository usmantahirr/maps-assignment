(function() {
  'use strict';

  angular
    .module('gmpsAssignment')
    .controller('SearchController', SearchController);

  /** @ngInject */
  function SearchController($scope, Station, MapConfFactory) {
    var vm = this;

    vm.stations = Station.all;
    vm.detail = {
      visible: false,
      content: {},
      gauge: {
        col: [{ id: 'In Transit', type: 'gauge' }],
        data: [{ 'In Transit': 0 }]
      }
    };

    vm.cb = function (chart) {
      vm.chart = chart;
    };

    $scope.$watch('search.selected', function (nv) {
      if (nv) {
        vm.detail.visible = true;
        vm.detail.content = nv;

        vm.detail.gauge.data[0]['In Transit'] = nv.used;

        if (vm.chart) {
          vm.chart.internal.config.gauge_max = nv.capacity;
        }
      }
    });

    vm.hide = function () {
      vm.detail.visible = false;
      vm.detail.content = NaN;
    };

    vm.getClass = function (percentage) {
      if (percentage === 0) {
        return 'label-danger'
      } else if (percentage < 50) {
        return 'label-warning'
      }
      return 'label-success'
    };
  }
})();

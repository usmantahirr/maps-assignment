(function() {
  'use strict';

  angular
    .module('gmpsAssignment')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log, $rootScope, $state, $document) {
    $log.debug('runBlock end');

    $rootScope.showSpinner = function () {
      $log.debug('loading');
      angular.element($document[0].querySelector(".page-loading")).removeClass("hidden");
      angular.element($document[0].querySelector("#ui-view")).addClass("hidden");
    };

    $rootScope.hideSpinner = function () {
      $log.debug('loaded');
      angular.element($document[0].querySelector(".page-loading")).addClass("hidden");
      angular.element($document[0].querySelector("#ui-view")).removeClass("hidden");
    };

    var stateChangeStartBroadcast = $rootScope.$on('$stateChangeStart', function(evt, to, params) {
      if (to.redirectTo) {
        evt.preventDefault();
        $state.go(to.redirectTo, params, { location: 'replace' })
      }

      if (to.resolve) {
        $rootScope.showSpinner();
      }
    });

    var stateChangeSuccessBroadcast = $rootScope.$on('$stateChangeSuccess', function(evt, to) {
      if (to.resolve) {
        $rootScope.hideSpinner();
      }
    });

    var stateChangeErrorBroadcast = $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
      event.preventDefault();
      $rootScope.hideSpinner();
      $state.go('error', {
        error: error
      });
    });

    $rootScope.$on('$destroy', function () {
      $log('destroyed', arguments);
      //remove the broadcast subscription when scope is destroyed
      stateChangeStartBroadcast();
      stateChangeSuccessBroadcast();
      stateChangeErrorBroadcast();
    });
  }

})();

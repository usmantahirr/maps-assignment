/* global _:false, Math:false, $:false */
(function() {
  'use strict';

  angular
    .module('gmpsAssignment')
    .constant('_', _)
    .constant('Math', Math)
    .constant('$', $)

})();

(function() {
  'use strict';

  angular
    .module('gmpsAssignment')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($log) {
    var vm = this;

    $log.debug(vm);
  }
})();

(function() {
  'use strict';

  angular
    .module('gmpsAssignment')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        redirectTo: 'welcome',
        views: {
          '@': {
            templateUrl: 'app/main/main.html',
            controller: 'MainController',
            controllerAs: 'main'
          }
        }
      })
      .state('error', {
        url: '/error',
        params: {
          error: null
        },
        views: {
          '@': {
            templateUrl: 'app/pages/error.html',
            controller: function ($log, $stateParams) {
              $log.debug('got into error', $stateParams.error);

              if ($stateParams.error) {
                if ($stateParams.error.status === -1 && $stateParams.error.config.method === "GET" ) {
                  this.error = {
                    message: 'No Internet or API Host down',
                    stack: $stateParams.error.config.url + ' not responding'
                  }
                } else {
                  this.error = {
                    message: $stateParams.error.message,
                    stack: $stateParams.error.stack
                  };
                }

              }
            },
            controllerAs: 'err'
          }
        }

      })
      .state('welcome', {
        url: 'welcome',
        parent: 'home',
        resolve: {
          Station: ['StationFactory', function(Station) {
            return Station.get();
          }]
        },
        views: {
          content: {
            templateUrl: 'app/pages/welcome/welcome.html',
            controller: ['$log', function ($log) {
              $log.debug('view data');
            }]
          }
        }
      })
      .state('mapView', {
        url: 'map-View',
        parent: 'home',
        resolve: {
          Station: ['StationFactory', function(Station) {
            return Station.get();
          }]
        },
        views: {
          content: {
            templateUrl: 'app/pages/mapView/mapView.html',
            controller: 'MapViewController',
            controllerAs: 'mv'
          }
        }
      })
      .state('distanceQuery', {
        url: 'distance-query',
        parent: 'home',
        resolve: {
          Station: ['StationFactory', function(Station) {
            return Station.get();
          }]
        },
        views: {
          content: {
            templateUrl: 'app/pages/distanceQuery/distanceQuery.html',
            controller: 'DistanceQueryController',
            controllerAs: 'dq'
          }
        }
      })
      .state('currentUsage', {
        url: 'usage',
        parent: 'home',
        resolve: {
          Station: ['StationFactory', function(Station) {
            return Station.get();
          }]
        },
        views: {
          content: {
            templateUrl: 'app/pages/currentUsage/currentUsage.html',
            controller: 'CurrentUsageController',
            controllerAs: 'cu'
          }
        }
      })
      .state('historicalUsage', {
        url: 'historical-usage',
        parent: 'home',
        resolve: {
          Station: ['StationFactory', function(Station) {
            return Station.get();
          }]
        },
        views: {
          content: {
            templateUrl: 'app/pages/historicalUsage/historicalUsage.html',
            controller: 'HistoricalUsageController',
            controllerAs: 'hu'
          }
        }
      })
      .state('search', {
        url: 'search',
        parent: 'home',
        resolve: {
          Station: ['StationFactory', function(Station) {
            return Station.get();
          }]
        },
        views: {
          content: {
            templateUrl: 'app/pages/search/search.html',
            controller: 'SearchController',
            controllerAs: 'search'
          }
        }
      });

    $urlRouterProvider.otherwise('/');
  }

})();

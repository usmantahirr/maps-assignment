(function() {
  'use strict';

  angular
    .module('gmpsAssignment')
    .config(config);

  /** @ngInject */
  function config($logProvider) {
    // Enable log
    $logProvider.debugEnabled(true);
  }

})();

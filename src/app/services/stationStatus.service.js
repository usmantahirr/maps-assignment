(function() {
  'use strict';

  angular
    .module('gmpsAssignment')
    .service('stationStatusSvc', stationStatusSvc);

  /** @ngInject */
  function stationStatusSvc($resource) {
    return $resource('https://gbfs.citibikenyc.com/gbfs/en/station_status.json');
  }
})();

(function() {
  'use strict';

  angular
    .module('gmpsAssignment')
    .service('stationInformationSvc', stationInformationSvc);

  /** @ngInject */
  function stationInformationSvc($resource) {
    return $resource('https://gbfs.citibikenyc.com/gbfs/en/station_information.json');
  }
})();

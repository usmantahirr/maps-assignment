(function() {
  'use strict';

  angular
    .module('gmpsAssignment', [
      'ngCookies',
      'ngResource',
      'ui.select',
      'ngSanitize',
      'ui.router',
      'ui.bootstrap',
      'ngMap',
      'gridshore.c3js.chart']
    );

})();

(function() {
  'use strict';

  angular
    .module('gmpsAssignment')
    .factory('StationFactory', Station);

  /** @ngInject */
  function Station($log, $q, stationStatusSvc, stationInformationSvc, _, Math) {
    var stationData = {
      info: NaN,
      status: NaN,
      all: NaN
    };

    function fetch(resolve, reject) {
      var p1 = stationInformationSvc.get().$promise;
      var p2 = stationStatusSvc.get().$promise;

      $log.info('Fetching Data');

      $q.all({
        info: p1,
        status: p2
      }).then(function (data) {
        stationData.info = data.info.data.stations;
        stationData.status = data.status.data.stations;
        stationData.all = getMerged();

        findUsagePercentages();

        $log.info('Fetch Complete');
        resolve(stationData);
      }).catch(function(err) {
        reject(err);
      });
    }

    function get() {
      var defer = $q.defer();

      if (stationData.info && stationData.status) {
        defer.resolve(stationData);
      } else {
        fetch(defer.resolve, defer.reject);
      }

      return defer.promise;
    }

    function findUsagePercentages() {
      var globalUsage = {
        totalAvailable: 0,
        totalDocksAvailable: 0,
        totalDocksDisabled: 0,
        totalCapacity: 0,
        totalUsed: 0
      };

      stationData.all.forEach(function (station) {
        station.percentage = Math.round((station.num_bikes_available / station.capacity) * 100);
        station.used =  station.capacity - station.num_bikes_available;

        globalUsage.totalAvailable += station.num_bikes_available;
        globalUsage.totalDocksAvailable += station.num_docks_available;
        globalUsage.totalDocksDisabled += station.num_docks_disabled;
        globalUsage.totalCapacity += station.capacity;
      });

      globalUsage.totalUsed = globalUsage.totalCapacity - globalUsage.totalAvailable;
      globalUsage.percentageAvailable = Math.round((globalUsage.totalAvailable / globalUsage.totalCapacity) * 100);
      stationData.globalUsage = globalUsage;
    }

    function getMerged() {
      return _.map(stationData.info, function(item) {
        return _.assign(item, _.find(stationData.status, ['station_id', item.station_id]));
      });
    }

    return {
      info: stationData.info,
      status: stationData.status,
      getMerged: getMerged,
      get: get
    }
  }
})();

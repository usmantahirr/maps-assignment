(function() {
  'use strict';

  angular
    .module('gmpsAssignment')
    .factory('MapConfFactory', MapConfFactory);

  /** @ngInject */
  function MapConfFactory() {
    var conf = {
      center: [40.727635, -73.993087],
      zoom: 14
    };

    function getIcon(percentage, cutOff1, cutOff2) {
      if (percentage === cutOff1) {
        return {
          url: 'assets/images/map_red.gif',
          scaledSize:[15, 20]
        }
      } else if (percentage < cutOff2) {
        return {
          url: 'assets/images/map_orange.gif',
          scaledSize:[15, 20]
        }
      }

      return {
        url: 'assets/images/map_green.gif',
        scaledSize:[15, 20]
      }
    }

    return {
      getConf: function () {return conf},
      getIcon: getIcon
    }
  }
})();
